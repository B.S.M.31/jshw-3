// 1. Що таке логічний оператор?
// Логічні оператори є важливими в JavaScript, оскільки вони дозволяють порівнювати змінні та виконувати певні дії на основі результату цього порівняння.

// 2. Які логічні оператори є в JavaScript і які їх символи?
// JavaScript надає три логічні оператори:

// ! (Логічне НЕ)
// || (Логічне АБО)
// && (Логічне І)






// Завдання 1

const userYear = parseInt(prompt ('Введіть свій вік'));

if (!isNaN(userYear)){
    if (userYear <= 12){
            alert('Ви дитина')
        } else if (userYear < 18){
            alert('Ви підліток')
        } else {
            alert('Ви дорослий')
        }
    }else{
        alert("Введіть свій вік")
}

// Завдання 2

const userNumber = prompt ('Напишіть місяць року').toLowerCase();

switch (userNumber) {
    case 'січень':
        alert('31 день')
        break
    case 'лютий':
        alert('28 днів, якщо високосний рік, то 29 днів')
        break
    case 'березень':
        alert('31 день')
        break
    case 'квітень':
        alert('30 день')
        break
    case 'травень':
        alert('31 день')
        break
    case 'червень':
        alert('30 день')
        break
    case 'липень':
        alert('31 день')
        break
    case 'серпень':
        alert('31 день')
        break
    case 'вересень':
        alert('30 день')
        break
    case 'жовтень':
        alert('31 день')
        break
    case 'листопад':
        alert('30 день')
        break
    case 'грудень':
        alert('31 день')
        break
    default:
        alert('Оберіть місяць')
}